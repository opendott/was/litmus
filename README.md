# Automated Google Image Litmus Test collection

This repository collects google image search screenshots for search queries (see `data/search.txt`).
The queries are automatically run every half hour using [Gitlab's](https://gitlab.com) continiuous integration.

For code, please the `master` branch: https://gitlab.com/opendott/was/litmus/-/tree/master

Data is collected in the `collection` branch: https://gitlab.com/opendott/was/litmus/-/tree/collection